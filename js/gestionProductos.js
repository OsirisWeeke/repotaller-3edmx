var productosObtenidos;

//funcion para obtener la lista de productos, se especifica con la url
function getProductos() {
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  //creamos un objeto tipo request
  var request=new XMLHttpRequest();
  //pre
  request.onreadystatechange=function() {
    if (this.readyState==4 && this.status==200) {
      //console.table(JSON.parse(request.responseText).value);
      productosObtenidos=request.responseText;
      procesarProductos();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarProductos() {
  var JSONProductos=JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla=document.getElementById("divTabla");
  var tabla=document.createElement("table");
  var tbody=document.createElement("tbody");
  for (var i = 0; i < JSONProductos.value.length; i++) {
    var nuevaFila=document.createElement("tr");

    var columnaNombre=document.createElement("td");
    columnaNombre.innerText=JSONProductos.value[i].ProductName;

    var columnaPrecio=document.createElement("td");
    columnaPrecio.innerText=JSONProductos.value[i].UnitPrice;

    var columnaStock=document.createElement("td");
    columnaStock.innerText=JSONProductos.value[i].UnitsInStock;


    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);

  tabla.classList.add("table");
  tabla.classList.add("table-striped");
}
//Las peticiones pasan por 4 estados
// 1 preparando peticiones
// 2 enviando
// 3 esperando respuesta
// 4 respondido
